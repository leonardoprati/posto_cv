import json

def load_source_subframe_dict(json_filename):
    with open(json_filename, 'r') as f:
        source_subframe_dict = json.load(f)
    return source_subframe_dict

# Dumps list of dict into json file
def save_source_subframe_dict(json_filename, dict_list):
    with open(json_filename, 'w') as f:
        json.dump(dict_list, f)   