from threading import Thread

class Classifier:

    # The model object is initialized outside classifier class.
    # This gives classifier more flexibility, but the model object 
    # must have at least a self.return_predict(self,image) method
    def __init__(self, model, frame_queue):

        # Setup
        self.model = model 
        self.input_queue = frame_queue
        
        # This class runs a threadified routine
        self.running = False
        self.thread = Thread(target=self.main_loop, args=())
        
    # Start thread activities
    def start(self):
        self.running = True
        self.thread.start()
 
    # Main loop. While running, tries to get images from input Q and classifies it
    # using provided model.
    # The results are put in a Q informed by the task requester.
    def main_loop(self):

        while self.running:
            # Get item from input queue
            (frame, requester_queue) = self.input_queue.get()

            # Test whether is empty or not
            if frame is None:
                    print("Got empty frame from queue")
                    break

            # Classify the frame and put results in the requester results queue
            results = self.model.return_predict(frame)
            requester_queue.put(results)

            # Notify requester that the frame results are avaliable
            requester_queue.task_done()

    # End threads activities
    def stop(self):
        self.thread.join()
        self.running = False