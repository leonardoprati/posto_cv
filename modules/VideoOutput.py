import cv2 as cv
import numpy as numpy
from threading import Thread

class VideoShow:
	def __init__(self, cam_detector_group_list):
		self.cam_detector_group_list = cam_detector_group_list
		self.running = False
		self.thread = None

	def start(self):
		self.thread = Thread(target=self.main_loop, args=())
		self.running = True
		self.thread.start()
	
	# BUG! 
	# QObject::~QObject: Timers cannot be stopped from another thread
	def stop(self):

		self.running = False
		self.thread.join()

	def is_running(self):
		return self.running

	def main_loop(self):

		mode = 'c'

		while self.running:
			window_id = 0

			for group in self.cam_detector_group_list:
		
				if '%c' % mode == 'c':			
					
					frame = group['cam'].read()
					cv.imshow('cam %d' % window_id, frame)
					window_id += 1

				elif '%c' % mode == 'd':
					for detector in group['detectors_list']:
						
						frame = detector.return_highlight()
						cv.imshow('detector %d' % window_id, frame)
						window_id += 1

				# Precisa de implementação
				# elif '%c' % mode 'h':		

				elif '%c' % mode == 'q' or mode == 27:
					cv.destroyAllWindows()
					self.running = False
				
				else:
					mode = 'c'
					frame = group['cam'].read()
					cv.imshow('cam %d' % window_id, frame)
			last_mode = mode
			
			mode = cv.waitKey(1)
			
			if mode == -1:
				mode = last_mode

			if mode is not last_mode:
				cv.destroyAllWindows()

	
# [Deprecated]
# class VideoRecorder:

# 	def __init__(self, destination_file, frame_width, frame_height):
# 		self.destination_file = destination_file
# 		self.writer = cv.VideoWriter(destination_file, cv.VideoWriter_fourcc('M','J','P','G'), 25.0, (frame_width,frame_height))
	
# 	def record_frame(self, frame):
# 		self.writer.write(frame)

# 	def close(self):
# 		self.writer.close()

