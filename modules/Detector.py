from skimage.measure import compare_ssim as ssim
from queue import Queue
from threading import Thread
import cv2 as cv

class Detector:

    # source_ is an previously initialized Cam object
    # subframe_points is a colection of 4 points, (y1, y2, x1, x2) delimiting the ROI
    # frame_q is a Queue shared by many Detectors where the GPU can collect frames to process
    # result_q is a Queue where the Detector object can colect it's individual results
    def __init__(self, source_cam, subframe_points, frame_q):
        print("DET: Stating detector at frame", subframe_points)
        # Setup 
        self.source = source_cam
        self.frame_points = subframe_points
        
        # !! This should be retrieved as parameter
        self.label_list = ['car','motorbike','truck','bus']

        self.thread = Thread(target=self.main_loop, args=())
        
        # CLAHE is a contrast enhancement method in opencv libs
        self.clahe = cv.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))

        # I/O config
        self.frame_q = frame_q
        self.result_q = Queue()
        
        # Initializing variables
        self.empty = False     # True if the frame has no vehicle in it
        self.running = False

        self.raw_frame = None
        self.update_frame()

        # The template_frame must be a empty frame
        # We start it as a copy of raw_frame, and update it while running
        self.template_frame = self.raw_frame.copy() 
        self.template_frame = self.apply_improc_operations(self.template_frame)

        # The highlighted frame shows a border arround indentified objects
        self.highlighted_frame = self.raw_frame.copy()
        
        # Count how many frames have been analysed consecutively without
        # big changes in similarity index  
        self.no_ssim_diff_count = 0

        # Asserts
        assert self.source.is_running(), "Source Cam is not running"

        size = self.raw_frame.shape[0:2]
        frame_width = size[1]
        frame_height = size[0]

        self.empty_count = 0
        self.last_p1 = 0
        self.last_p2 = 0
        self.last_p_text = 0
        self.last_text_to_put = 0



    def start(self):
        # Start the main loop thread after object setup is done    
        self.running = True
        self.thread.start()
        return self

    def stop(self):
        self.running = False  
        self.thread.join()
     
        

    # Extract the ROI from the frame.
    # This is needed because each source Cam can be read by many detectors,
    # each one looking for a different ROI.
    def get_roi(self, raw):
        return raw[self.frame_points[2]:self.frame_points[3], self.frame_points[0]:self.frame_points[1]]

    # Update raw frame with Cam's reading
    def update_frame(self):

        # Get ROI from source frame
        try:
            raw_frame = self.source.read()
        except Exception as e:
            print(e.message, e.args)

        raw_frame = self.get_roi(raw_frame)
        # Update object member
        self.raw_frame = raw_frame

    # Returns the current raw_frame
    def return_frame(self):
        return self.raw_frame

    # Returns the current template_frame
    def return_template(self):
        return self.template_frame

    def return_highlight(self):
        return self.highlighted_frame    

    # Return measures of similarity between the current frame and the template
    def compare_with_template(self, frame):
        
        # Structural SIMilarity index between the frame and the template
        ssim_index = ssim(self.template_frame, frame)
        
        # Pixel-by-pixel subtraction
        delta = cv.absdiff(frame, self.template_frame)
        
        # Highlight pixels with changes above some threshold value
        thresh = cv.threshold(delta, 25, 255, cv.THRESH_BINARY)[1]
        thresh = cv.dilate(thresh,None,iterations=2)

        return ssim_index, delta, thresh

    def update_template(self, frame):
        self.template_frame = frame

    # Find center and area of connected pixels regions
    # To be applied in the threshold generated binary frame.
    def find_objects(self, thresh_frame):        
        
        im, contours, hierarchy = cv.findContours(thresh_frame, 1, 2)

        obj_list = []
        min_area = 2000

        for contour in contours:

            # Check if object has a minimum area
            area = cv.contourArea(contour)
            if area > min_area:

                # Calculate object center coordinates
                x, y, w, h = cv.boundingRect(contour)
                xm = x + (w/2)
                ym = y + (h/2)

                # Append to the list
                obj_list.append((xm, ym, area))
        return obj_list     

    # Apply gray-level conversion, contrast enhancement and gaussian blur
    def apply_improc_operations(self, frame):
        img = cv.cvtColor(frame.copy(), cv.COLOR_BGR2GRAY)
        img = self.clahe.apply(img)
        img = cv.GaussianBlur(img, (9,9), 0)
        return img

    def is_empty(self):
        return self.empty 

    # Routine to run in thread
    def main_loop(self):
        
        while self.running:
 
            # Update frame and pre process it
            self.update_frame()
            
            self.frame_q.put( (self.raw_frame, self.result_q) )
            results = self.result_q.get(block=True)
            self.check_results_for_labels(results)
            
    def check_results_for_labels(self, results_dict_list):
        for element in results_dict_list:
            if element['label'] in self.label_list:

                self.empty = False
                
                # Draw a coloured rectangle arroud found object
                p1 = (element['topleft']['x'], element['topleft']['y'])
                p2 = (element['bottomright']['x'], element['bottomright']['y'])
                p_text = (element['topleft']['x']+4, element['bottomright']['y']-4)
                self.highlighted_frame = cv.rectangle(self.raw_frame.copy(), p1, p2,(0,0,255),2)
                
                # Write its label
                text_to_put = "{0}: {1}".format(element['label'], ('%.3f' % element['confidence']))
                self.highlighted_frame = cv.putText(self.highlighted_frame, text_to_put, p_text, cv.FONT_HERSHEY_SIMPLEX, 0.7, (0,0,255),2)
            
                # Backup last point
                self.last_p1 = p1
                self.last_p2 = p2
                self.last_p_text = p_text
                self.last_text_to_put = text_to_put

            else:
                self.empty = True
