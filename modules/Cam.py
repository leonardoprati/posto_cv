from threading import Thread
import cv2 as cv

# Threadified video capture class
class Cam:

    # src is the videocapture source, as defined by opencv doc:
    # https://docs.opencv.org/2.4/modules/highgui/doc/reading_and_writing_images_and_video.html#videocapture-videocapture
    def __init__(self, src):
        print("CAM: Starting source ", src)
        # Capture the stream
        self.stream = cv.VideoCapture(src)
        assert self.stream.isOpened(), "CAM: Source not opened!"

        self.running = False
        self.thread = Thread(target=self.main_loop, args=())


        # Read first frame
        (self.grab, self.frame) = self.stream.read()
        assert self.grab, "CAM: Could not grab first frame!"

    # Reading a frame from the camera is a blocking process.
    # So we want to threadify it, to avoid blocking the main thread.
    # This method starts the object's thread.
    def start(self):
        
        self.running = True
        self.thread.start()

    # This is the threadified routine.
    # While not asked to stop, it will continuosly try to read the stream.
    def main_loop(self):

        while self.running:

            try:
                # Reads the next frame
                (self.grab, self.frame) = self.stream.read()
            except Exception as e:
                print(e.message, e.args)

    def is_running(self):
        return self.running

    # Reading the CAM object returns the lastest read frame
    def read(self):
        return self.frame

    # Asks CAM to stop
    def stop(self):
        self.running = False
        self.thread.join()
        self.stream.release()

# Utility function
def test_sources(list_of_source_paths):

    print("Checking sources for validity:")
    for source in list_of_source_paths:
        print("Checking: ", source)
        is_opened = cv.VideoCapture().open(source)

        if is_opened:
            print("Success!")
        else:
            print("Failure!") 
