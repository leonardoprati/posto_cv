import json
import sys
import os 

darkflow_path = '/home/darkflow'
sys.path.insert(0,darkflow_path)

from darkflow.net.build import TFNet


class TFyolo:

    def __init__(self, parameters_filepath):

        # Reads parameter from json file
        with open(parameters_filepath, 'r') as f: 
            options = f.read()
            self.options = json.loads(options)
        print(self.options)

        # Creates internal model
        self.network = TFNet(self.options)

    # Any model class should have at least this method
    def return_predict(self, input_image):
        return self.network.return_predict(input_image)
