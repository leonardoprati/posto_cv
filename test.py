from queue import Queue
import cv2 as cv

from modules.Cam import Cam
from modules.Detector import Detector
from modules.VideoOutput import VideoShow
from modules.Model import TFyolo
from modules.Classifier import Classifier
import modules.Configs

source_subframe_list = modules.Configs.load_source_subframe_dict('./data/sources.json')
active_cam_detectors_groups = []

subframe_queue = Queue()

for element in source_subframe_list:

    # Create cams
    stream_source = element['source']
    cap = Cam(stream_source)
    cap.start()

    # Create detectors
    detectors_list = []
    
    for subframe in element["subframe_list"]:
        
        det = Detector(cap, subframe, subframe_queue)
        det.start()
        detectors_list.append(det)

    # Bundle source and detectors in a dict
    cap_detector_dict = {'cam': cap, 'detectors_list': detectors_list}
    active_cam_detectors_groups.append(cap_detector_dict)

model = TFyolo("./data/model_options.cfg")
yolo_classifier = Classifier(model, subframe_queue)
yolo_classifier.start()

# Visualization thread scratch
visualization = VideoShow(active_cam_detectors_groups)

visualization.start()
while visualization.is_running():
    pass
visualization.stop()

# Cleaning 
for element in active_cam_detectors_groups:

    for detector in element['detectors_list']:
        detector.stop()

    element['cam'].stop()
